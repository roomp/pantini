export default {
  head: {
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
    ],
  },
  css: [
    '@/assets/scss/main.scss',
    '@/assets/fonts/font-awesome-4.7.0/scss/font-awesome.scss'
  ],
  modules: [
    '@nuxtjs/style-resources'
  ],

  styleResources: {
    scss: [
      '@/assets/scss/_vars.scss'
    ]
  },
}